const http = require("http");
const uuid4 =require("uuid4")


const myServer=http.createServer((req,res)=>{
    if (req.url==="/html" && req.method==="GET") {
        res.writeHead(202,{"Content-Type":"text/html"})
        res.end(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
        
          </body>
        </html>`)
    }else if (req.url==="/json"&& req.method==="GET") {
        res.writeHead(202,{"Content-Type":"application/json"})
        res.end(`{
            "slideshow": {
              "author": "Yours Truly",
              "date": "date of publication",
              "slides": [
                {
                  "title": "Wake up to WonderWidgets!",
                  "type": "all"
                },
                {
                  "items": [
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                  ],
                  "title": "Overview",
                  "type": "all"
                }
              ],
              "title": "Sample Slide Show"
            }
          }`)
    }else if (req.url==="/uuid"&& req.method==="GET") {
        res.writeHead(202,{"Content-Type":"application/json"})
        let uuid=uuid4() 
        let obj={"uuid":uuid} 
        res.end(JSON.stringify(obj));
    }else if (req.url.includes("/status") && req.method ==="GET") {
        const status=req.url.split("/")[2]
        res.writeHead(Number(status),{"Content-Type":"text/plain"})
        res.end(`status_code==${status}`)

    }else if (req.url.includes("/delay") && req.method ==="GET") {
        const delay=req.url.split("/")[2]

        setTimeout(()=>{
            res.writeHead(200,{"Content-Type":"text/plain"})
            res.end("Jai Shree Ram")
        },delay*1000)
    }
})

myServer.listen(8000,()=>console.log("Server started"));